FROM ubuntu:jammy AS builder

ENV DEBIAN_FRONTEND='noninteractive'

# Make build folder, install requirements
RUN mkdir -p /build && \
    apt update && \
    apt upgrade -y -o Dpkg::Options::="--force-confold" && \
    apt install -y --no-install-recommends \
    automake \
    build-essential \
    ca-certificates \
    git \
    libboost-filesystem-dev \
    libboost-iostreams-dev \
    libboost-thread-dev \
    libcurl4-gnutls-dev \
    libdb++-dev \
    libssl-dev \
    libtool \
    libzip-dev \
    pkg-config \
    vim \
    wget \
    # also brought in as dependency of one of the above
    zlib1g-dev
# Cleanup is not required, as the builder image is throw-away anyway

ENV CFLAGS="-O3 -pipe -march=x86-64 -mtune=generic"
ENV CXXFLAGS="-O3 -pipe -march=x86-64 -mtune=generic"
ENV LDFLAGS="-Wl,-O1,--as-needed,-z,now"


# Do the actual build
WORKDIR /build
RUN git clone https://github.com/gridcoin/Gridcoin-Research && \
    cd Gridcoin-Research && \
    git checkout 5.4.8.0 && \
    ./autogen.sh && \
    ./configure \
    --with-incompatible-bdb \
    --disable-tests \
    --disable-gui-tests \
    --with-gui=no \
    --without-miniupnpc \
    --without-qrencode \
    --disable-dependency-tracking \
    && \
    make


# RUNNER IMAGE - KEEP LIGHTWEIGHT!
FROM ubuntu:jammy as runner

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["gridcoinresearchd"]

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    curl \
    gosu \
    libboost-filesystem1.74.0 \
    libboost-iostreams1.74.0 \
    libboost-thread1.74.0 \
    libcurl4 \
    libcurl4-gnutls-dev \
    libdb5.3++ \
    libzip4 && \
    useradd -d /home/grc -U -m grc && \
    ln -s /entrypoint.sh /usr/local/bin/cli && \
    ln -s /entrypoint.sh /usr/local/bin/asgrc && \
    apt clean && \
    apt autoclean -y && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /home/grc
VOLUME ["/home/grc"]
VOLUME ["/var/lib/boinc"]

COPY --from=builder /build/Gridcoin-Research/src/gridcoinresearchd /usr/local/bin/
COPY --from=builder /build/Gridcoin-Research/doc/gridcoinresearch.1 /usr/local/man/man1/
COPY --from=builder /build/Gridcoin-Research/doc/gridcoinresearchd.1 /usr/local/man/man1/


