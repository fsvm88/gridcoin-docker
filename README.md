Minimal docker image to run Gridcoin fullnode

## Quick guide
```
docker run -d \
  --name gridcoin_cont \
  -v /path/to/blockchain/dir:/home/grc \
  -v /path/to/boinc/dir:/var/lib/boinc \
  -p 32749:32749 \
  registry.gitlab.com/fsvm88/gridcoin-docker:latest
```

Notes:
- /path/to/blockchain/dir is the path that will contain the .GridcoinResearch folder
- /path/to/boinc/dir is your /var/lib/boinc folder
- entrypoint script will change permissions on your blockchain dir as required (at each start of the container)
- config file is expected at /path/to/blockchain/dir/.GridcoinResearch/gridcoinresearch.conf

## Using the CLI
The entrypoint provides a nice way to interact with the running daemon via the CLI: `docker exec -it gridcoin_cont cli help`.  
I usually put in my .bashrc `alias grccli='docker exec -it gridcoin_cont cli'` and use it as `grccli getmininginfo`.

### User-mode commands
You can also execute commands as user:group (`grc:grc`) running in the container via `docker exec -it gridcoin_cont asgrc <command>`, although this command was mainly used for development.
